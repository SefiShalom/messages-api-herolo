const mongoose = require('mongoose');

/*
a message contains :

1. sender (owner)
2. receiver
3. message
4. subject
5. creation date

*/

const Message = new mongoose.Schema({
    sender: {
        type: String,
        required: true
    },
    receiver: {
        type: String,
        required: true
    },
    message: {
        type: String,
        required: true
    },
    subject: {
        type: String,
        required: true
    },
    creation_date: {
        type: Date,
        default: Date.now()
    },
    is_read: {
        type: Boolean,
        default: false
    },
    deleted: {
        type: Boolean,
        default: false
    }
});


module.exports = mongoose.model('Message', Message);
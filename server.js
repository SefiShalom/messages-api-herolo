const express = require("express");
const server = express();
const PORT = process.env.PORT || 3000;
const mongoose = require("mongoose");
mongoose.Promise = require("bluebird");
const bodyParser = require("body-parser");

const messages_routes = require("./messages_routes");
const users_routes = require("./users_routes");

const db = mongoose.connection;
const db_username = process.env.DB_USERNAME;
const db_password = process.env.DB_PASS;

const db_uri = `mongodb://${db_username}:${db_password}@cluster0-shard-00-00-d0iqf.mongodb.net:27017,cluster0-shard-00-01-d0iqf.mongodb.net:27017,cluster0-shard-00-02-d0iqf.mongodb.net:27017/Messages?replicaSet=Cluster0-shard-0&ssl=true&authSource=admin`;

try {
  mongoose.connect(db_uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
} catch (err) {
  console.error(err.code);
}

db.on("error", console.error.bind(console, "MongoDB connection error!\n"));

server.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

server.use(bodyParser.json());

server.use("/messages", messages_routes);
server.use("/users", users_routes);

server.listen(PORT, () =>
  console.log(`messages api app is listening on port ${PORT}`)
);


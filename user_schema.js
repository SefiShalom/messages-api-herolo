const mongoose = require("mongoose");

const User = new mongoose.Schema({
  user_name: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    require: true,
  },
  token: String,
});

module.exports = mongoose.model("User", User);

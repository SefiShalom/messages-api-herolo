const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
router.use(bodyParser.json());

//authentication
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const User = require("./user_schema.js");
const ObjectId = require("mongoose").Types.ObjectId;

const salt = bcrypt.genSaltSync();

router.post("/login", (req, res) => {

  try {
    let user_name = req.body.user_name;
    let hashed_password = bcrypt.hashSync(req.body.password, salt);

    let user_obj = {
      user_name: user_name,
      password: hashed_password
    }

    User.findOne(user_obj, (err, user) => {
      if (err) throw err;
      if (user) {
        res.status(200).send({status: 1, message:'user found', data:{_id: user._id, token: user.token}});
      } else {
        User.findOne({user_name: user_name}, (err, user) => {
          if(err) throw err;
          if(user){
            res.status(400).send({status: 0, message:'wrong password', data:{}});
          }else{
            let token = jwt.sign(user_obj, process.env.JWT_SECRET || "secretkey", {expiresIn: "24h"})
            user_obj.token = token;
            new_user = new User(user_obj);
            new_user.save((err) => {
            if (err) throw err;
            res.status(201).send({status: 1, message:'user created', data:{_id: new_user._id, token: new_user.token}});
            });
          }
        });
      }
    });
  } catch (err) {
    console.error(err);
    res.status(400).send(err);
  }
});

module.exports = router;
const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
router.use(bodyParser.json());

const Message = require("./message_schema.js");
const User = require("./user_schema.js");
const message_schema = require("./message_schema.js");

const ObjectId = require("mongoose").Types.ObjectId;

/*
The rest API should contains :
- Write message
- Get all messages for a specific user
- Get all unread messages for a specific user
- Read message (return one message)
- Delete message (as owner or as receiver)
*/

router.post("/write-message", (req, res) => {
  try {

    let message = new Message({
      sender: ObjectId(req.body.sender),
      receiver: ObjectId(req.body.receiver),
      message: req.body.message,
      subject: req.body.subject,
    });

    message.save((err) => {
      if (err) throw err;
      console.log(message);
      res.status(201).send(message);
    });

  } catch (err) {
    console.error(err);
    res.status(400).send(err);
  }
});

router.get("/all-user-messages", (req, res) => {

  let access_token = (req.body && req.body.access_token) ||
                    (req.query && req.query.access_token) ||
                    req.headers['x-access-token'];
  try {
    User.findOne({token: access_token}, (err, user)=>{
      if(err) throw err;
      if(user){
        let filter = {
          $or: [{sender: user._id}, {receiver: user._id}],
          deleted: false
        };
        Message.find(filter, (err, messages) => {
          if (err) throw err;
          res.status(200).send(messages);
        });
      }
    });
  } catch (err) {
    console.error(err);
    res.status(400).send(err);
  }
});

router.get("/all-user-unread-messages", (req, res) => {

  let access_token = (req.body && req.body.access_token) ||
                      (req.query && req.query.access_token)||
                        req.headers['x-access-token'];
  try {
    User.findOne({token: access_token}, (err, user)=>{
      if(err) throw err;
      if(user){
        let filter = {
          $or: [{sender: user._id}, {receiver: user._id}],
          is_read: false,
          deleted: false
        };

        Message.find(filter, (err, messages) => {
          if (err) throw err;
          res.status(200).send(messages);
        });
      }
    });
  } catch (err) {
    console.error(err);
    res.status(400).send(err);
  }
});

router.put("/read-message", (req, res) => {
  try {
    let message_id = ObjectId(req.query.message_id);
    let filter = {
      _id: message_id,
      is_read: false,
      delete: false,
    };

    let update = {
      is_read: true,
    };

    Message.updateOne(filter, update, (err, data) => {
      if (err) throw err;
      console.log(data);
      res.status(200).send(data);
    });
  } catch (err) {
    console.error(err);
    res.status(400).send(err);
  }
});

router.delete("/delete-message", (req, res) => {
  try {
    console.log(req.query.message_id);
    let message_id = ObjectId(req.query.message_id);
    let filter = {_id: message_id};
    let update = { deleted: true };

    Message.updateOne(filter, update, (err, data) => {
      if (err) throw err;
      console.log(data)
      res.status(200).send(data);
    });
  } catch (err) {
    console.error(err);
    res.status(400).send(err);
  }
});

module.exports = router;